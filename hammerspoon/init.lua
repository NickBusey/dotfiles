require 'autoreload'
require 'caffeine'
require 'expander'
require 'mouse'
require 'power'
require 'wifi'
require 'windows'

-- Add paste spoofing
hs.hotkey.bind({"cmd", "shift"}, "V", function() hs.eventtap.keyStrokes(hs.pasteboard.getContents()) end)