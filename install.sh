rm -rf ~/.hammerspoon ~/.tmuxinator

ln -s $(pwd)/hammerspoon/  ~/.hammerspoon
ln -s $(pwd)/tmuxinator/ ~/.tmuxinator

ln -sF $(pwd)/tmux.conf ~/.tmux.conf
ln -sF $(pwd)/zshrc ~/.zshrc
ln -sF $(pwd)/vimrc ~/.vimrc
ln -sF $(pwd)/yabairc ~/.yabairc
ln -sF $(pwd)/skhdrc ~/.skhdrc

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# git clone https://github.com/jeffreytse/zsh-vi-mode \
#  $ZSH_CUSTOM/plugins/zsh-vi-mode

